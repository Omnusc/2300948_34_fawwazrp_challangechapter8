import logo from './logo.svg';
import './App.css';
import React, { useState } from "react";
import Header from "./components/Header";
import Edit from "./components/Edit";
import EditForm from "./components/EditForm";
import RegistrationForm from './components/RegistrationForm';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  const [playerData, setPlayerData] = useState([]);
  const handleFormSubmit = (newPlayer) => {
    setPlayerData([...playerData, newPlayer]);
  };
  return (
    <div className="App">

<Router>
      <Header />
      <RegistrationForm onFormSubmit={handleFormSubmit} />
      <Edit playerData={playerData} />
      
  <Routes>
    <Route path="/EditForm" element={<EditForm />} />
  </Routes>
</Router>
 
    </div>
  );
}

export default App;
