import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input, Label, Button } from "reactstrap";

function EditForm() {
  // Define local state variables for player data
  const [player, setPlayer] = useState({
    id: localStorage.getItem("id"),
    username: localStorage.getItem("username"),
    email: localStorage.getItem("email"),
    experience: localStorage.getItem("experience"),
    lvl: localStorage.getItem("lvl"),
  });

  // Handler for form input changes
  const handleInputChange = (e) => {
    const { name, value } = e.target;

    // Update the player object with new data
    setPlayer({ ...player, [name]: value });
  };

  // Handler for form submission
  const handleSubmit = (e) => {
    e.preventDefault();

    // Update player's data in localStorage
    localStorage.setItem("username", player.username);
    localStorage.setItem("email", player.email);
    localStorage.setItem("experience", player.experience);
    localStorage.setItem("lvl", player.lvl);
  };

  return (
    <div>
      <h1>Edit Player</h1>
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Label htmlFor="username">Username</Label>
          <Input
            type="text"
            id="username"
            name="username"
            value={player.username}
            onChange={handleInputChange}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="email">Email</Label>
          <Input
            type="email"
            id="email"
            name="email"
            value={player.email}
            onChange={handleInputChange}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="experience">Experience</Label>
          <Input
            type="number"
            id="experience"
            name="experience"
            value={player.experience}
            onChange={handleInputChange}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="lvl">Level</Label>
          <Input
            type="number"
            id="lvl"
            name="lvl"
            value={player.lvl}
            onChange={handleInputChange}
          />
        </FormGroup>
        <Button type="submit">Save Changes</Button>
      </Form>
    </div>
  );
}

export default EditForm;
