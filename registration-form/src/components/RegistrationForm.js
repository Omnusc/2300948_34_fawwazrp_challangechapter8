import React, { useState } from "react";
import { Form, FormGroup, Input, Label, Button, Table } from "reactstrap";
import "../App.css"


function RegistrationForm({ onFormSubmit })
{ 

  const [searchUsername, setSearchUsername] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const [searchEmail, setSearchEmail] = useState("");
  // const [searchEmailResut, setSearchEmailResult] = useState([]);
    
  const [initialForm] = useState(
  {
    id: 0,
    username: "",
    email: "",
    experience: "",
    lvl: ""
  }
  )

  function passPlayer(id, username, email, experience , lvl)
  {
    localStorage.setItem("id", id);
    localStorage.setItem("username", username);
    localStorage.setItem("email", email);
    localStorage.setItem("experience", experience);
    localStorage.setItem("lvl", lvl);
  }

  const [formData, setFormData] = useState({ ...initialForm });

  // make array for player data
  const [playerData, setPlayerData] = useState([]);

  // to hide the result before pressing submit
  const [isSubmitted, setIsSubmitted] = useState(false);

  // this will be called when the user submits the form.
  const handleSubmit = (e) =>  
  {
    e.preventDefault();
    // show input in console
    if 
    ( 
      !formData.username ||
      !formData.email ||
      !formData.experience ||
      !formData.lvl
    )
    {
      // Show an error message or take any other action to handle the validation error
      alert("Please fill in all fields");
      return;
    }
    const newPlayer = 
    {
      id: playerData.length + 1,
      username: formData.username,
      email: formData.email,
      experience: formData.experience,
      lvl: formData.lvl,
    }
    // Add the new player to the playerData array
    setPlayerData([...playerData, newPlayer]);

    // Reset the form data and mark as submitted
    // setFormData({ ...initialForm });
    if (onFormSubmit) 
    {
      onFormSubmit(newPlayer);
    }

    setIsSubmitted(true);
  } 
  const handleInputChange = (e) => 
  {
    // Reset the isSubmitted state when any input field is changed
    setIsSubmitted(false);

    // Update the corresponding form data field
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  // Search logic username
  // thx chatGPT
  const handleSearchUsernameChange = (e) => 
  {
    setSearchUsername(e.target.value);
  };

  const handleSearchSubmit = (e) => 
  {
    e.preventDefault();
    const filteredPlayers = playerData.filter((player) =>
      player.username.toLowerCase().includes(searchUsername.toLowerCase())
    );
    setSearchResults(filteredPlayers);
  }; 

  // search Email
  const handleSearchEmailChange = (e) =>
  {
    setSearchEmail(e.target.value);
  };

  const handleSearchEmailSubmit = (e) => {
    e.preventDefault();
      
    const filteredPlayers = playerData.filter((player) =>
      player.email.toLowerCase().includes(searchEmail.toLowerCase())
    );
    
  setSearchResults(filteredPlayers);
  };

  return(

    <div className="container mt-2 mb-5">
        <Form onSubmit={handleSubmit}>
            <FormGroup>

            <div className="mb-3 mt-3 row">
                <Label htmlFor="username" className="col-sm-1 col-form-label">
                    Username
                </Label>
                <div className="col-sm-11">
                    <Input type="text"className="form-control"id="username"placeholder="Input your username"
                    value={formData.username} name="username"
                    // so if the input is updated, the result is hidden again
                    onChange = {handleInputChange}/>
                </div>
            </div>
            <div className="mb-3 mt-3 row">
                <Label htmlFor="email" className="col-sm-1 col-form-label">
                    Email
                </Label>
                <div className="col-sm-11">
                    <Input type="email"className="form-control"id="email"placeholder="Input your email"
                    value={formData.email} name="email"
                    // so if the input is updated, the result is hidden again
                    onChange = {handleInputChange}/>
                </div>
            </div>
            <div className="mb-3 mt-3 row">
                <Label htmlFor="experience" className="col-sm-1 col-form-label">
                    Experience
                </Label>
                <div className="col-sm-11">
                    <Input type="number"className="form-control"id="experience"placeholder="Input your experience"
                    value={formData.experience} name="experience"
                    // so if the input is updated, the result is hidden again
                    onChange = {handleInputChange}/>
                </div>
            </div>
            <div className="mb-3 mt-3 row">
                <Label htmlFor="lvl" className="col-sm-1 col-form-label">
                    lvl
                </Label>
                <div className="col-sm-11">
                    <Input type="number"className="form-control"id="lvl"placeholder="Input your lvl"
                    value={formData.lvl} name="lvl"
                    // so if the input is updated, the result is hidden again
                    onChange = {handleInputChange}/>
                </div>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
            </FormGroup>
        </Form>
      
        {isSubmitted && 
        (
            <div>
                <h2>Success</h2>
            </div>
        )}
      {/* Search Form */}
      <Form onSubmit={handleSearchSubmit}>
        <FormGroup>
          <div className="mb-3 mt-3 row">
            <Label htmlFor="searchUsername" className="col-sm-1 col-form-label">
               Username
            </Label>
            <div className="col-sm-11">
            <Input
                  type="text"
                  className="form-control"
                  id="searchUsername"
                  placeholder="Search by username"
                  value={searchUsername}
                  onChange={handleSearchUsernameChange}
            />
            </div>
          </div>
          <Button type="submit" className="btn btn-primary">
                  Search
          </Button>
        </FormGroup>
      </Form>
      <Form>

      </Form>
      {searchResults.length > 0 && 
      (
        <div>
          <h2>Search Results:</h2>
          {searchResults.map((player, index) => (
            <div key={index}>
                <p>id: {player.id}</p>
                <p>Username: {player.username}</p>
                <p>Email: {player.email}</p>
                <p>Experience: {player.experience}</p>
                <p>lvl: {player.lvl}</p>
              {/* Display search results */}
            </div>
          ))}
        </div>
      )}

{/* {searchResults.length > 0 && (
  <div>
    <h2>Search Results:</h2>
    {searchResults.map((player, index) => (
      <div key={index}>
        <p>Player {index + 1}:</p>
        <p>id: {player.id}</p>
        <p>Username: {player.username}</p>
        <p>Email: {player.email}</p>
        <p>Experience: {player.experience}</p>
        <p>lvl: {player.lvl}</p>
      </div>
    ))}
  </div>
)} */}

  </div> // container
    
)
}
export default RegistrationForm;