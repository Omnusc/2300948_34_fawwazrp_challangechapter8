import React from "react";
import { useNavigate, Link } from "react-router-dom";
import "../App.css";
// Importing playerData here if it's not defined in this file
import { Button, Table } from "react-bootstrap";

function Edit({ playerData }) {
  function setPlayer(id, username, email, exp, lvl) {
    localStorage.setItem("id", id);
    localStorage.setItem("username", username);
    localStorage.setItem("email", email);
    localStorage.setItem("experience", exp);
    localStorage.setItem("lvl", lvl);
  }

  return (
    // Conditionally render the content based on playerData.length
    playerData.length > 0 && (
      <div>
        <nav className="">
          <div className="row col-12 d-flex justify-content-center text-black">
            <h3>Edit</h3>
          </div>
        </nav>
        <div>
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Experience</th>
                <th>Level</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              {playerData.map((player) => (
                <tr key={player.id}>
                  <td>{player.username}</td>
                  <td>{player.email}</td>
                  <td>{player.experience}</td>
                  <td>{player.lvl}</td>
                  <td>
                    <Link to="/EditForm">
                      <Button
                        onClick={() =>
                          setPlayer(
                            player.id,
                            player.username,
                            player.email,
                            player.experience,
                            player.lvl
                          )
                        }
                        variant="info"
                      >
                        Edit Player
                      </Button>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    )
  );
}

export default Edit;
